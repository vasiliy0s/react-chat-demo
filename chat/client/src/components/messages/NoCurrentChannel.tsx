import React from 'react'
import './NoCurrentChannel.scss'

const NoCurrentChannel: React.FC = () => {
  return <div className="no-content">
    <span className="no-current-channel__hand">{'\u261A'}</span>
    Choose channel
  </div>
}

export default NoCurrentChannel