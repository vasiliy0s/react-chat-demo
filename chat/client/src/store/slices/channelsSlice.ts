import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { listChannels } from '../../api/channelsApi'
import ChannelModel from '../../models/ChannelModel'
import { RootState } from '../store'
import { LoadingStatus } from './LoadingStatus'

export interface ChannelsState {
  channels: ChannelModel[]
  currentChannel: ChannelModel | null
  channelsLoadingStatus: LoadingStatus
}

const initialState: ChannelsState = {
  channels: [],
  currentChannel: null,
  channelsLoadingStatus: LoadingStatus.Idle,
}

export const loadChannels = createAsyncThunk(
  'channels/loadChannels',
  listChannels
)

export const channelsSlice = createSlice({
  name: 'channels',
  initialState,
  reducers: {
    setCurrentChannel: (state, { payload }: PayloadAction<ChannelModel>) => {
      state.currentChannel = payload
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadChannels.pending, (state) => {
        state.channelsLoadingStatus = LoadingStatus.Loading
      })
      .addCase(loadChannels.fulfilled, (state, action) => {
        state.channelsLoadingStatus = LoadingStatus.Idle
        state.channels = action.payload
      })
  },
})

export const { setCurrentChannel } = channelsSlice.actions

export const getChannels = (state: RootState) => state.channels.channels
export const getIsChannelsLoading = (state: RootState) => state.channels.channelsLoadingStatus === LoadingStatus.Loading
export const getCurrentChannel = (state: RootState) => state.channels.currentChannel

export default channelsSlice.reducer
