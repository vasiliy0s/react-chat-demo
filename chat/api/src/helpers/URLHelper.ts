import express from 'express'

export default class URLHelper {
  static firstQueryParam = (req: express.Request, key: string): string | null => {
    let value = req.query[key]
    if (Array.isArray(value)) value = value[0]
    if (typeof value === 'string') return value
    return null
  }
}