import express from 'express'

export default abstract class BaseController {
  protected wrap(cb: express.Handler) {
    return async (...params: Parameters<express.Handler>) => {
      try {
        // FIXME: better type inference to allow callbacks be async
        await cb(...params)
      } catch(err) {
        params[2](err)
      }
    }
  }

  protected ok<DTO>(res: express.Response, dto?: DTO) {
    if (dto != null) {
      res.type('application/json')
      return res.status(200).json(dto)
    }
    return res.status(200).end()
  }

  protected created<DTO>(res: express.Response, dto?: DTO) {
    if (dto != null) {
      res.type('application/json')
      return res.status(201).json(dto)
    }
    return res.status(201).end()
  }

  protected notFound(res: express.Response) {
    return this.text(res, 404, 'Not found')
  }

  protected internalError(res: express.Response) {
    res.type('text/plain')
    return this.text(res, 500, 'Internal server error')
  }

  protected text(res: express.Response, status: number, text: string) {
    return res.status(status).send(text)
  }
}