import express from 'express'
import { Server } from 'http'
import cors from 'cors'
import router from './routes'
import errorController from './controllers/ErrorController'

interface AppConfig {
  port: number
}

export function runServer ({ port }: AppConfig) {
  return express()
    .use(cors())
    .use('/', router)
    .use(errorController.handler)
    .listen(port, () => {
      console.log('Server listening on http://localhost:%s', port)
    })
}

export function getGracefulShutdown(server: Server) {
  return function gracefulShutdownHandler() {
    console.log('\nShutting down the server...')
    server.close(() => {
      console.log('\nServer stopped\n')
      process.exit(0)
    })
  }
}

