import React, { useEffect } from 'react'
import ChannelMessages from './ChannelMessages'
import MessageForm from './MessageForm'
import './Channel.scss'
import { useAppDispatch, useAppSelector } from '../store/hooks'
import { getCurrentChannel } from '../store/slices/channelsSlice'
import NoCurrentChannel from './messages/NoCurrentChannel'
import { getIsMessagesLoading, getMessagesByChannelIdDesc, loadMessages, sendMessage } from '../store/slices/messagesSlice'

const Channel: React.FC = () => {
  const currentChannel = useAppSelector(getCurrentChannel)
  const isMessagesLoading = useAppSelector(getIsMessagesLoading)
  const getMessagesSelector = useAppSelector(getMessagesByChannelIdDesc)
  const dispatch = useAppDispatch()

  const currentChannelId = currentChannel?.channelId
  const messages = currentChannelId != null ? getMessagesSelector(currentChannelId) : []

  useEffect(() => {
    if (currentChannelId) {
      dispatch(loadMessages(currentChannelId))
    }
  }, [dispatch, currentChannelId])

  function onSend(text: string) {
    if (!currentChannelId) return
    dispatch(sendMessage({ text, channelId: currentChannelId }))
  }

  if (!currentChannel) return <NoCurrentChannel />

  return <div className="channel">
    <div className="channel__messages">
      <ChannelMessages key={currentChannelId} loading={isMessagesLoading} messages={messages} />
    </div>
    <MessageForm key={currentChannelId} onSend={onSend} />
  </div>
}

export default Channel