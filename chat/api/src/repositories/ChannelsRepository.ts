import { ChannelModel } from '../models/ChannelModel'
import InMemoryRepository from './base/InMemoryRepository'
import faker from 'faker'

export class ChannelsRepository extends InMemoryRepository<ChannelModel> {
  async findById(channelId: string): Promise<ChannelModel | null> {
    const [channel] = await this.filterBy(ch => ch.channelId === channelId, 1, 0)
    return channel || null
  }
}

const channelsRepository = new ChannelsRepository(
  new Array(10).fill(null).map(() =>
    new ChannelModel(faker.lorem.word())
  )
)

export default channelsRepository