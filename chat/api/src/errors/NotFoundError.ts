import BaseError from './base/BaseError'

export default class NotFoundError extends BaseError {
  constructor() {
    super(404, 'Not found')
  }
}