export default class TypeConvertionHelper {
  static strToIntSafe(str: string | null | undefined): number | null {
    if (str == null) return null
    const value = Number.parseInt(str, 10)
    if (Number.isNaN(value)) return null
    return value
  }
}