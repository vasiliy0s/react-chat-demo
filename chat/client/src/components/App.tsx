import React from 'react'
import Channel from './Channel'
import ChannelsList from './ChannelsList'
import Layout from './Layout'

function App() {
  return (
    <Layout>
      <ChannelsList />
      <Channel />
    </Layout>
  );
}

export default App;
