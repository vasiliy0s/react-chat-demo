import BaseError from './base/BaseError'

export default class BadRequestError extends BaseError {
  constructor() {
    super(400, 'Bad request')
  }
}