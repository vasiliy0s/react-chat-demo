import React from 'react'
import ChannelModel from '../models/ChannelModel'
import './ChannelListItem.scss'

interface Props {
  channel: ChannelModel
  onSelect: (ch: ChannelModel) => void
  isActive?: boolean
}

const ChannelListItem: React.FC<Props> = (props) => {
  return <div className={`channel-list-item ${props.isActive ? 'channel-list-item--active' : ''}`} onClick={() => props.onSelect(props.channel)}>
    <div className={`channel-list-item__name ${props.isActive ? 'channel-list-item__name--active' : ''}`}>
      {props.channel.name}
    </div>
  </div>
}

export default ChannelListItem