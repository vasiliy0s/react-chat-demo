import React, { ChangeEvent, KeyboardEvent, useState, MouseEvent } from 'react'
import './MessageForm.scss'

interface Props {
  onSend(text: string): void
  sending?: boolean
}

const MessageForm: React.FC<Props> = (props) => {
  const [text, setText] = useState('')

  function send() {
    if (props.sending) return
    const trimmedText = text.trim()
    if (trimmedText.length) props.onSend(trimmedText)
    setImmediate(() => setText(''))
  }

  function onTextChange(evt: ChangeEvent<HTMLTextAreaElement>) {
    setText(evt.target.value)
  }

  function onTexareaKeydown(evt: KeyboardEvent) {
    if (evt.key === 'Enter') {
      evt.stopPropagation()
      send()
    }
  }

  function onClickButton(evt: MouseEvent) {
    evt.preventDefault()
    send()
  }

  return <div className="message-form">
    <textarea
      className="message-form__input"
      onKeyDown={onTexareaKeydown}
      value={text}
      onChange={onTextChange}
      disabled={props.sending}
      maxLength={1000}
    />
    <button
      disabled={props.sending || !text}
      onClick={onClickButton}
      className="message-form__send-button"
    >
      Send
    </button>
  </div>
}

export default MessageForm