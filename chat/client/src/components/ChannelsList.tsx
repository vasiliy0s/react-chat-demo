import React, { useEffect } from 'react'
import ChannelModel from '../models/ChannelModel'
import { useAppDispatch, useAppSelector } from '../store/hooks'
import { loadChannels, setCurrentChannel, getChannels, getIsChannelsLoading, getCurrentChannel } from '../store/slices/channelsSlice'
import ChannelListItem from './ChannelListItem'
import './ChannelsList.scss'

const ChannelsList: React.FC = (props) => {
  const channels = useAppSelector(getChannels);
  const isChannelsLoading = useAppSelector(getIsChannelsLoading);
  const currentChannel = useAppSelector(getCurrentChannel);
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(loadChannels())
  }, [dispatch])

  function selectChannel(ch: ChannelModel) {
    dispatch(setCurrentChannel(ch))
  }

  function isCurrentChannel(ch: ChannelModel) {
    return ch.channelId === currentChannel?.channelId
  }

  return <div className="channels-list">
    {isChannelsLoading ? (
      <p>Loading channels...</p>
    ) : channels.map(ch => (
      <ChannelListItem key={ch.channelId} channel={ch} isActive={isCurrentChannel(ch)} onSelect={selectChannel} />
    ))}
  </div>
}

export default ChannelsList