import React, { useEffect, useRef } from 'react'
import MessageModel from '../models/MessageModel'
import NewMessageModel from '../models/NewMessageModel'
import ChannelMessage from './ChannelMessage'
import './ChannelMessages.scss'
import LoadingMessages from './messages/LoadingMessages'
import NoMessages from './messages/NoMessages'

interface Props {
  loading?: boolean
  messages: Array<MessageModel | NewMessageModel>
}

const ChannelMessages: React.FC<Props> = ({ messages, loading }) => {
  const containerRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (!loading && messages.length) {
      if (containerRef.current) {
        containerRef.current?.scroll(0, containerRef.current.clientHeight)
      }
    }
  }, [messages, loading, containerRef])

  return <div className="channel-messages">
    <div className="channel-messages__container" ref={containerRef}>
      {messages.map(msg => (
        <ChannelMessage key={'localMessageId' in msg ? msg.localMessageId : msg.messageId} message={msg} />
      ))}
      {loading ? (
        <LoadingMessages />
      ) : (
        !messages.length && <NoMessages />
      )}
    </div>
  </div>
}

export default ChannelMessages