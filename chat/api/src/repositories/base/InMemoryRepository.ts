interface Filter<DTO> {
  (item: DTO): boolean
}

export default abstract class InMemoryRepository<DTO> {
  constructor(private collection: DTO[] = []) {}

  async list(limit: number, offset: number): Promise<DTO[]> {
    return this.collection.slice(offset, limit)
  }

  async filterBy(filter: Filter<DTO>, limit: number, offset: number): Promise<DTO[]> {
    const found: DTO[] = []
    let count = 0
    for (const item of this.collection) {
      if (filter(item)) {
        if (offset > 0) offset--
        else {
          found.push(item)
          count++
        }
      }
      if (count >= limit) break
    }
    return found
  }

  async push(item: DTO): Promise<void> {
    this.collection.push(item)
  }
}