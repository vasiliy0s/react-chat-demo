import express from 'express'
import TypeConvertionHelper from '../helpers/TypeConvertationHelper'
import URLHelper from '../helpers/URLHelper'
import channelsRepository, { ChannelsRepository } from '../repositories/ChannelsRepository'
import BaseController from './base/BaseController'

class ChannelsController extends BaseController {
  constructor(
    private channelsRepo: ChannelsRepository
  ) {
    super()
  }

  listChannels = this.wrap(async (req: express.Request, res: express.Response) => {
    const limit = TypeConvertionHelper.strToIntSafe(URLHelper.firstQueryParam(req, 'limit'))
    const offset = TypeConvertionHelper.strToIntSafe(URLHelper.firstQueryParam(req, 'offset'))
    const channels = await this.channelsRepo.list(limit || 10, offset || 0)
    return this.ok(res, channels)
  })
}

const channelsController = new ChannelsController(channelsRepository)

export default channelsController