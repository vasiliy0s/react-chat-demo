import { Router, json } from 'express'
import channelsController from './controllers/ChannelsController'
import messagesController from './controllers/MessagesController'

export default Router()
  .get('/channels', channelsController.listChannels)
  .get('/messages/:channelId', messagesController.listChannelMessages)
  .post('/:channelId', json(), messagesController.addChannelMessage)
