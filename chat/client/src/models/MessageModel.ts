export default interface MessageModel {
  messageId: string
  createdAt: string
  text: string
  channelId: string
}