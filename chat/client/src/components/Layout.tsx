import React from 'react'
import "./Layout.scss"

const Layout: React.FC = (props) => {
  return <main className="layout-main">
    {props.children}
  </main>
}

export default Layout