import { runServer, getGracefulShutdown } from './server'

const server = runServer({ port: 3030 })

process.on('SIGTERM', getGracefulShutdown(server))
process.on('SIGHUP', getGracefulShutdown(server))
process.on('SIGINT', getGracefulShutdown(server))