import MessageModel from '../models/MessageModel'

export async function postMessage(text: string, channelId: string): Promise<MessageModel> {
  const res = await fetch(`${process.env.REACT_APP_API_URL}/${channelId}?limit=99999`, {
    method: 'POST',
    body: JSON.stringify({ text }),
    headers: {
      // 'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
  const message = await res.json()
  return message
}

export async function listChannelMessages(channelId: string): Promise<MessageModel[]> {
  const res = await fetch(`${process.env.REACT_APP_API_URL}/messages/${channelId}`)
  const messages: MessageModel[] = await res.json()
  return messages
}