export default abstract class BaseError extends Error {
  protected constructor(
    public statusCode: number,
    public text: string
  ) {
    super(text)
  }
}