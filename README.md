# React Chat Demo

![Chat app](./images/preview.png)

## How to start the stack

_WARNING_: All shell commands are suitable for macOS or Linux (i.e., Ubuntu) because this is my preferred OS(es).

### Install proper Node.js version (if missed)

Please ensure you're on the same [Node.js](https://nodejs.org/en/download/) version, or higher:

```sh
node --version
```

Check the Node.js version in `./.nvmrc` file and set the proper version via [NVM](https://github.com/nvm-sh/nvm) tool (or in your preferred way of node.js versions management):

```sh
nvm use
# If there is no matched local version, you can install it with
cat .nvmrc | nvm install $1
```

### Install Yarn

If you don't use [Yarn](https://yarnpkg.com/getting-started/install) please install it since this project uses Yarn lock files to deliver consisten versions

```sh
npm install -g yarn
```

### Install dependencies

Since this project was build from two parts - API on Node.js and Client on React.js - and both of them should be shipped with the single repository, I choose to employ [Lerna](https://lerna.js.org/) for management of both projects like in a monorepository:

```sh
yarn bootstrap # which will run `npx lerna bootstrap` to install everything you need
```

### Run the project

...with a single command

```sh
yarn start
```

This command will start both the client and backend for you. Your favorite (or, default) browser will be open automatically at <http://localhost:3000/>.

### If you'd develop or run apps separately

...then this is not a problem:

```sh
# Run client with command:
cd chat/client && npm start
# and the same for the backend:
cd chat/api && npm start
```

## Things to change/improve

1. Routing with channel ID or slug (name), e.g. localhost:3000/channels/my-channel

It is not necessary to use routing in the current task but it will be needed in the future for such kinds of application because saving state via URL is a good practice.

2. Immediate information posting via Web Socket

I didn't found the requirement about any new messages notifying mechanism implementation (correct me please if I was wrong), but in instant messgener app users are asking about immediate messages showing (because they are 'instant') and notifying, so there are two ways for this:

- [WebSocket API](https://developer.mozilla.org/ru/docs/Web/API/WebSockets_API), usually implemented via well-known [Socket.io](https://socket.io/),
- [WebRTC](https://developer.mozilla.org/ru/docs/Web/API/WebRTC_API), which allows peer-to-peer connection and less server interaction.

So I decided to not wasting the time on this since this is not mandatory, but it's core feature of such kind of apps.

3. Paginatio middleware

On the API I started using pagination params like `offset` & `limit` for future messages loading by slices, which is not neccessary on this application. I don't want to remove this because this is an important feature for future application evolving. But the current implementation is also looks not good - at least I'd move the code parsing this parameters to a middleware function instead of parsing it manually each time, with better validation (like responding with `400` error for non-numerical limit params).

4. Lerna is also not the neccessary tool here...

...but it useful to run both apps simultanteiously and provides good basis for structuring this app like a monorepo.

5. Node.js version management

NVM is not a perfect tool nowadays when we have everything put into the Docker, but it's enough for such a small progamm. So I'd note that it's better to use environment normalization with `node_modules` directoy sharing via Docker & docker-compose tools.

6. Environment variables for all apps

Here we can see `.env.development` [dotenv](https://www.npmjs.com/package/dotenv) file in `chat/client` which ships out-of-the box within `react-scripts` pacakge but there are nothing the same for API so thats better to either:

- Add the dotenv to `chat/api` for envars like running `port`, or...
- Use standartized solution which allow us keeping configuration in the repository root, probably with dotenv files too.

7. Running API via ts-node

This is not a good option since each start requires TS->JS->Native code compilation and it's better to compile TS to JS (at least, or better to the bytecode) before starting. But it's okay for development and such a simple MVP.

8. DI system on API

The [DI](https://en.wikipedia.org/wiki/Dependency_injection) system is completely missed in pursue to save my time, but it's a good pattern for TS appliations on Node.js. There are different [IoC](https://en.wikipedia.org/wiki/Inversion_of_control) containers can be considered for this architecture pattern applying.

9. CSS modules on Client

Of course the current approach with direct styles injection with globally-visible class names is weird because usually a lot of compoonents can have the same selectors. To avoid this we can easily switch to CSS Modules feature which is going [out-of-the box](https://create-react-app.dev/docs/adding-a-css-modules-stylesheet/) with ``react-scripts`.

10. Better components structure on Client

Since the current structure is not ideal and was a product of hackaton-alike product it's better to consider for aligning some components stucturing approach (probably, DDD will be better one, but another approaches also exists).
