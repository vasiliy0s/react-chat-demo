export default interface ChannelModel {
  channelId: string
  name: string
}