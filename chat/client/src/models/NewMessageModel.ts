import MessageModel from './MessageModel'

export default interface NewMessageModel extends Pick<MessageModel, 'text' | 'channelId'> {
  localMessageId: string
  localCreatedAt: string
}
