import * as uuid from 'uuid'

export class ChannelModel {
  channelId = uuid.v4()

  constructor(
    public name: string
  ) {}
}