import ChannelModel from '../models/ChannelModel'

export async function listChannels(): Promise<ChannelModel[]> {
  const res = await fetch(`${process.env.REACT_APP_API_URL}/channels?limit=99999`)
  const channels: ChannelModel[] = await res.json()
  return channels
}