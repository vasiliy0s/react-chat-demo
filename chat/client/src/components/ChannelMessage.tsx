import React, { useMemo } from 'react'
import { format, formatDistanceToNow } from 'date-fns'
import MessageModel from '../models/MessageModel'
import NewMessageModel from '../models/NewMessageModel'
import './ChannelMessage.scss'

interface Props {
  message: MessageModel | NewMessageModel
}

const ChannelMessage: React.FC<Props> = ({ message }) => {
  const key = 'localMessageId' in message ? message.localMessageId : message.messageId
  const createdAt = useMemo(
    () => {
      const date = new Date('createdAt' in message ? message.createdAt : message.localCreatedAt)
      return format(date, "dd MMMM yyyy, HH:mm:ss")
    },
    [message]
  )
  const createdAtFromNow = useMemo(
    () => {
      const date = new Date('createdAt' in message ? message.createdAt : message.localCreatedAt)
      return formatDistanceToNow(date)
    },
    [message]
  )

  return <div className="channel-message" key={key}>
    <div className="channel-message__text">
      {message.text}
    </div>
    <div className="channel-message__meta-container">
      <time title={createdAt}>{createdAtFromNow}</time>
    </div>
  </div>
}

export default ChannelMessage