import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import channelsSlice from './slices/channelsSlice'
import messagesSlice from './slices/messagesSlice'

export const store = configureStore({
  reducer: {
    channels: channelsSlice,
    messages: messagesSlice,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
