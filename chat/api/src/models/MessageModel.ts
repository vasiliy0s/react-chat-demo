import * as uuid from 'uuid'

export default class MessageModel {
  messageId = uuid.v4()
  createdAt = new Date().toISOString()

  constructor(
    public text: string,
    public channelId: string
  ) {}
}