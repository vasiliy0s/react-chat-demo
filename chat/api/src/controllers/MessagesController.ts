import express from 'express'
import BaseController from './base/BaseController'
import BadRequestError from '../errors/BadRequestError'
import messagesRepository, { MessagesRepository } from '../repositories/MessagesRepository'
import channelsRepository, { ChannelsRepository } from '../repositories/ChannelsRepository'
import URLHelper from '../helpers/URLHelper'
import TypeConvertionHelper from '../helpers/TypeConvertationHelper'
import MessageModel from '../models/MessageModel'
import NotFoundError from '../errors/NotFoundError'

class MessagesController extends BaseController {
  constructor(
    private messagesRepository: MessagesRepository,
    private channelsRepository: ChannelsRepository
  ) {
    super()
  }

  listChannelMessages = this.wrap(async (req: express.Request, res: express.Response) => {
    const { channelId } = req.params
    if (!channelId) throw new BadRequestError()

    const limit = TypeConvertionHelper.strToIntSafe(URLHelper.firstQueryParam(req, 'limit'))
    const offset = TypeConvertionHelper.strToIntSafe(URLHelper.firstQueryParam(req, 'offset'))
    const messages = await this.messagesRepository.listByChannelId(channelId, limit || Number.MAX_SAFE_INTEGER, offset || 0)
    return this.ok(res, messages)
  })

  addChannelMessage = this.wrap(async (req: express.Request, res: express.Response) => {
    const { text } = req.body
    if (text == null) throw new BadRequestError()

    const { channelId } = req.params
    if (!channelId) throw new BadRequestError()
    const channel = await this.channelsRepository.findById(channelId)
    if (channel == null) throw new NotFoundError()

    const message = new MessageModel(text, channelId)
    await this.messagesRepository.push(message)
    return this.created(res, message)
  })
}

const messagesController = new MessagesController(messagesRepository, channelsRepository)

export default messagesController