import express from 'express'
import BaseError from '../errors/base/BaseError'
import BaseController from './base/BaseController'

export class ErrorController extends BaseController {
  handler = (err: BaseError | Error, _req: express.Request, res: express.Response, _next: Function) => {
    console.error(err)

    if ('statusCode' in err) {
      return this.text(res, err.statusCode, err.text)
    }

    return this.internalError(res)
  }
}

const errorController = new ErrorController()

export default errorController