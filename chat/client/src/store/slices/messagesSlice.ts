import { RootState } from './../store'
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { postMessage, listChannelMessages } from '../../api/messagesApi'
import MessageModel from '../../models/MessageModel'
import NewMessageModel from '../../models/NewMessageModel'
import { LoadingStatus } from './LoadingStatus'
import * as uuid from 'uuid'

export interface MessagesState {
  messages: Record<string, Array<NewMessageModel | MessageModel>>
  messagesLoadingStatus: LoadingStatus
}

type NewMessageParams = Pick<NewMessageModel, 'text' | 'channelId'>

const initialState: MessagesState = {
  messages: {},
  messagesLoadingStatus: LoadingStatus.Idle
}

export const loadMessages = createAsyncThunk(
  'messages/loadMessages',
  async (channelId: string) => {
    const messages = await listChannelMessages(channelId)
    return {
      messages,
      channelId
    }
  }
)

export const sendMessage = createAsyncThunk(
  'messages/sendMessage',
  async ({ text, channelId }: NewMessageParams, thunkApi) => {
    const localMessageId = uuid.v4()
    const localCreatedAt = new Date().toISOString()
    thunkApi.dispatch(addMessage({
      text,
      channelId,
      localMessageId,
      localCreatedAt
    }))
    const message = await postMessage(text, channelId)
    return {
      message,
      localMessageId,
    }
  }
)

export const messagesSlice = createSlice({
  name: 'messages',
  initialState,
  reducers: {
    addMessage(state, { payload: message }: PayloadAction<NewMessageModel>) {
      const { channelId } = message
      if (!(channelId in state.messages)) state.messages[channelId] = []
      state.messages[channelId].push(message)
      sendMessage(message)
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadMessages.pending, (state) => {
        state.messagesLoadingStatus = LoadingStatus.Loading
      })
      .addCase(loadMessages.fulfilled, (state, { payload: { messages, channelId } }) => {
        state.messagesLoadingStatus = LoadingStatus.Idle
      if (!(channelId in state.messages)) state.messages[channelId] = []
        state.messages[channelId] = messages
      })
      .addCase(sendMessage.fulfilled, (state, { payload: { message, localMessageId } }) => {
        const {channelId} = message
        for (let i = state.messages[channelId].length - 1; i >= 0; i--) {
          const msg = state.messages[channelId][i]
          if ('localMessageId' in msg && msg.localMessageId === localMessageId) {
            state.messages[channelId][i] = message
            break;
          }
        }
      })
  }
})

const { addMessage } = messagesSlice.actions

export const getMessagesByChannelIdDesc = (state: RootState) =>
  (channelId: string) => {
    return (state.messages.messages[channelId] || [])
      .slice()
      .sort(sortMessagesByCreatedAtDesc)
  }
export const getIsMessagesLoading = (state: RootState) => state.messages.messagesLoadingStatus === LoadingStatus.Loading

export default messagesSlice.reducer

function sortMessagesByCreatedAtDesc(a: MessageModel | NewMessageModel, b: MessageModel | NewMessageModel) {
  const aTS = new Date('createdAt' in a ? a.createdAt : a.localCreatedAt)
  const bTS = new Date('createdAt' in b ? b.createdAt : b.localCreatedAt)
  return aTS.getTime() - bTS.getTime()
}