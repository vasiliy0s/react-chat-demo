import MessageModel from '../models/MessageModel'
import InMemoryRepository from './base/InMemoryRepository'

export class MessagesRepository extends InMemoryRepository<MessageModel> {
  async listByChannelId(channelId: string, limit: number, offset: number) {
    return this.filterBy(msg => msg.channelId === channelId, limit, offset)
  }
}

const messagesRepository = new MessagesRepository()

export default messagesRepository