import React from 'react'

const NoMessages: React.FC = () => {
  return <div className="no-content">
    <span>No messages in this channel</span>
  </div>
}

export default NoMessages