import React from 'react'

const LoadingMessages: React.FC = () => {
  return <div className="no-content">
    <span>Loading channel messages...</span>
  </div>
}

export default LoadingMessages